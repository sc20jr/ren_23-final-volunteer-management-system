from flask import Flask
from templates.admin.routes import admin_blueprint
from templates.website.routes import website_blueprint
from templates.authentication.routes import auth_blueprint
from flask_wtf.csrf import CSRFProtect
from flask_toastr import Toastr
from datetime import datetime,timedelta
# from config import DATABASE_URL
from apis.controller import api_controller
# Initialize the Flask application
app = Flask(__name__,
            static_folder='assets',
            template_folder='templates')
toastr = Toastr(app)
app.secret_key = 'volunterrXyz'
csrf = CSRFProtect(app)
app.register_blueprint(api_controller, url_prefix='/api')
app.register_blueprint(website_blueprint)
app.register_blueprint(auth_blueprint)
app.register_blueprint(admin_blueprint)
csrf.exempt(api_controller)
def timeago(value):
    timestamp = datetime.strptime(value, '%Y-%m-%d %H:%M:%S')
    current_time = datetime.utcnow()
    time_difference = current_time - timestamp
    if time_difference < timedelta(minutes=1):
        return 'just now'
    elif time_difference < timedelta(hours=1):
        minutes = int(time_difference.total_seconds() / 60)
        return f'{minutes} minutes ago'
    elif time_difference < timedelta(days=1):
        hours = int(time_difference.total_seconds() / 3600)
        return f'{hours} hours ago'
    else:
        days = int(time_difference.total_seconds() / 86400)
        return f'{days} days ago'
    
app.jinja_env.filters['timeago'] = timeago
# Start the application
if __name__ == '__main__':
    app.run(host='0.0.0.0',port='5000', debug=True)
