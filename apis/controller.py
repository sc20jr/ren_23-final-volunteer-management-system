from flask import Blueprint,jsonify,request
import jwt
import traceback
from config import JWT_SECRET_KEY
# from apis.database import get_db
from urllib.parse import parse_qs
from apis.authmiddleware import authMiddleware
import json
import sqlite3
from flask import g
from config import DATABASE_URL
from werkzeug.utils import secure_filename
import os
def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE_URL)
        db.row_factory = sqlite3.Row
    return db
api_controller = Blueprint('api_controller', __name__)
# @api_controller.before_request
# def middleware():
#     if "auth" not in request.path:
#         authMiddleware()
@api_controller.route('auth/authenticate',methods = ['POST'])
def authenticate():
    db = get_db()
    body = request.get_json()
    userInfo = db.execute('''SELECT u.id as user_id,u.name,u.email,u.phone_number,
                          u.role_id,u.isBlocked,u.isSystemManager,
                          u.isVolunteer,u.isOrganization,
                          (CASE WHEN u.profile_picture is not null THEN '/'||m.url||'/'||m.name ELSE '' END) as profile_picture
                          FROM users as u
                          LEFT JOIN medias as m on m.id = u.profile_picture
                          where email = ? and password = ?''',(body['email'],body['password'])).fetchone()
    if(userInfo):
        # if(userInfo["isBlocked"] == 1):
        #     return jsonify({"success":False,"message":"You are currently blocked"}),200
        access_token = jwt.encode({"user_id":userInfo['user_id'],"role_id":userInfo["role_id"],},JWT_SECRET_KEY,algorithm="HS256")
        data = {
            "access_token":access_token,
            "userInfo": {
                "user_id":userInfo['user_id'],
                "name":userInfo['name'],
                "email":userInfo['email'],
                "phone_number":userInfo["phone_number"],
                "role_id":userInfo["role_id"],
                "isBlocked":userInfo["isBlocked"],
                "isSystemManager":userInfo["isSystemManager"],
                "isVolunteer":userInfo["isVolunteer"],
                "isOrganization":userInfo["isOrganization"],
                "profile_picture":userInfo["profile_picture"]
            }
        }
        return jsonify({"success":True,"data":data,"message":"Login Sucessfully"}),200
    else:
        return jsonify({"success":False,"message":"Invalid Credentials"}),200
@api_controller.route('auth/sign-up',methods = ['POST'])
def signUp():
    db = get_db()
    body = request.get_json()
    userInfo = db.execute('SELECT id as userId FROM users where email = ? or phone_number = ?',(body['email'],body['phone_number'])).fetchone()
    roleInfo = db.execute('SELECT id as roleId FROM roles WHERE id = ?',(body['role_id'],)).fetchone()
    if(roleInfo is None): 
        return jsonify({"success":False,"message":"Role doesnot Exists"}),200
    if(userInfo):
        return jsonify({"success":False,"message":"Email or phone_number already Exists"}),200
    else:
        try:
            c = db.cursor()
            c.execute('INSERT INTO users (name, email,phone_number,password,role_id) VALUES (?,?,?,?,?)', (body['name'], body['email'], body['phone_number'],body['password'],body['role_id']))
            c.execute('INSERT INTO user_detail (user_id) VALUES (?)', (c.lastrowid,))
            db.commit()
            # Check if the user was inserted into the database
            result = db.execute('SELECT id as userId FROM users WHERE email = ?', (body['email'],)).fetchone() 
            if result:
                return jsonify({"success":True,"message":"User Created Successfully"}),200
            else:
                return jsonify({"success":False,"message":"SomeThing went wrong for creating user"}),200
        except Exception as error:
            c.execute('ROLLBACK')
            return jsonify({"success":False,"message":str(error),"exception":traceback.format_exc()}),500
            
@api_controller.route('events/add-event',methods = ['POST'])
@authMiddleware
def addEvent(current_user):
    db = get_db()
    body = request.get_json()
    if(current_user['user_id'] != body['organization_id']):
            return jsonify({"success":False,"message":"UnAuthorized"}),401
    organizationInfo = db.execute('SELECT u.id as organization_id,r.name as roleName FROM users as u JOIN roles r on u.role_id = r.id where u.id = ? and r.id = 2',(current_user['user_id'],)).fetchone()
    if (organizationInfo is None):
        return jsonify({"success":False,"message":"Only organization member post a job"}),200
    else:
        c = db.cursor()
        c.execute('BEGIN')
        try:
            c.execute('INSERT INTO medias (name,size,file,created_by,created_by) VALUES (?,?,?,?,?)', 
                    (body['fileName'], body['size'], body['image'],current_user['user_id'],current_user['user_id']))
            c.execute('INSERT INTO events (title,description,website,organization_id,start_date,end_date,status,max_application,duration,emptype,qualification,experience,location,isUrgent,media_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', 
                    (body['title'], body['description'],body['website'], current_user['user_id'],body['start_date'],body['end_date'],"requiring",body['max_application'],body['duration'],body['emptype'],body['qualification'],body['experience'],body['location'],body['isUrgent'],c.lastrowid))
            db.commit()
            return jsonify({"success":True,"message":"Job Posted Successfully"}),200
        except Exception as error:
            c.execute('ROLLBACK')
            return jsonify({"success":False,"message":str(error),"exception":traceback.format_exc()}),500

@api_controller.route('events/update-event',methods = ['POST'])
@authMiddleware
def updateEvent(current_user):
    db = get_db()
    body = request.get_json()
    if(current_user['user_id'] != body['organization_id']):
            return jsonify({"success":False,"message":"UnAuthorized"}),401
    organizationInfo = db.execute('SELECT u.id as organization_id,r.name as roleName FROM users as u JOIN roles r on u.role_id = r.id where u.id = ? and r.id = 2',(current_user['user_id'],)).fetchone()
    if (organizationInfo is None):
        return jsonify({"success":False,"message":"Only organization member post a job"}),200
    else:
        c = db.cursor()
        c.execute('BEGIN')
        try:
            if("old_image" in body):
                c.execute('UPDATE medias set file = ? where id = ?',(body["old_image"],body["media_id"]))
            else:
                c.execute('UPDATE medias set name = ? , size = ? , file = ? where id = ?',(body["name"],body["size"],body["image"],body["media_id"]))
            c.execute('''UPDATE events 
                               SET title = ? ,
                               description = ?,
                               website = ?,
                               start_date = ?,
                               end_date = ?,
                               status = ?,
                               max_application = ?,
                               duration = ?,
                               emptype = ?,
                               qualification = ?,
                               experience = ?,
                               location = ?,
                               status = ?,
                               isUrgent = ?,
                               updated_at=date("now") where id = ?''', 
                    (body['title'], body['description'],body['website'],body['start_date'],body['end_date'],
                     "requiring",body['max_application'],body['duration'],body['emptype'],
                     body['qualification'],body['experience'],body['location'],body['status'],body['isUrgent'],body['event_id']))
            db.commit()
            records = c.execute('SELECT e.id,e.title,e.description,e.website,e.start_date,e.end_date,e.organization_id,e.status,e.max_application,m.file as image,m.name,m.id as media_id FROM events AS e JOIN medias m ON m.id = e.media_id where e.id = ?',(body["event_id"],)).fetchone()
            return jsonify({"success":True,"data":dict(records),"message":"Job Posted Successfully"}),200
        except Exception as error:
            c.execute('ROLLBACK')
            return jsonify({"success":False,"message":str(error),"exception":traceback.format_exc()}),500
@api_controller.route('events/delete-event',methods = ['POST'])
@authMiddleware
def deleteEvent(current_user):
    db = get_db()
    body = request.get_json()
    if(current_user['user_id'] != body['organization_id']):
            return jsonify({"success":False,"message":"UnAuthorized"}),401
    organizationInfo = db.execute('SELECT u.id as organization_id,r.name as roleName FROM users as u JOIN roles r on u.role_id = r.id where u.id = ? and r.id = 2',(current_user['user_id'],)).fetchone()
    if (organizationInfo is None):
        return jsonify({"success":False,"message":"Only organization member post a job"}),200
    else:
        db.execute('UPDATE events SET isDeleted = ?  where id = ?', 
                    (1,body['event_id']))
        db.commit()
        return jsonify({"success":True,"message":"Event Deleted Successfully"}),200
@api_controller.route('events/get-all-events-by-organization',methods = ['POST'])
@authMiddleware
def getAllEventByOrganization(current_user):
    db = get_db()
    body = request.get_json()
    body['limit'] = body['limit'] if body['limit']==10 else body['limit']
    body['page'] = body['page'] if body['page']==1 else body['page']
    records = db.execute('''SELECT 
                            e.id,e.title,e.description,e.website,e.start_date,e.end_date,e.organization_id,
                            e.status,e.max_application,
                            e.duration,e.emptype,e.qualification,e.experience,e.location,e.isUrgent
                            FROM events AS e 
                            WHERE e.organization_id = ? and e.isDeleted = ? and e.status LIKE ?
                            order by e.id desc limit ? offset ?''',(
                              current_user['user_id'],
                              0,
                              '%%'if body['filter']=='all' else '%'+body['filter']+'%',
                              body['limit'],
                              (body['page']-1)*body['limit'],)).fetchall()
    result = []
    if(len(records)):
        for row in records:
            result.append(dict(row))
        return jsonify({"success":True,"data": result,"message":"Record Found"}),200
    return jsonify({"success":False,"message":"No Record Found"}),200

@api_controller.route('events/get-all-events',methods = ['POST'])
@authMiddleware
def getAllEvent(current_user):
    db = get_db()
    body = request.get_json()
    body['limit'] = body['limit'] if body['limit']==10 else body['limit']
    body['page'] = body['page'] if body['page']==1 else body['page']
    records = db.execute(
        '''SELECT e.id,e.title,e.description,e.website,e.start_date,
        e.end_date,e.organization_id,e.status,e.max_application,
        m.file as image,m.name,m.id as media_id , u.name as companyName,
        e.duration,e.emptype,e.qualification,e.experience,e.location,e.isUrgent,
        ifnull((SELECT CASE WHEN a.volunteer_id THEN 1 ELSE 0 END from applicants as a where a.volunteer_id = ? and a.event_id=e.id ),0) as isApplied
        FROM events AS e JOIN medias m ON m.id = e.media_id
        JOIN users u ON u.id = e.organization_id
    
        WHERE e.isDeleted = ? and e.status!='end' order by e.id desc limit ? offset ?''',
        (current_user['user_id'],0,body['limit'],(body['page']-1)*body['limit'],)).fetchall()
    result = []
    if(len(records)):
        for row in records:
            result.append(dict(row))
        return jsonify({"success":True,"data": result,"message":"Record Found"}),200
    return jsonify({"success":False,"message":"No Record Found"}),200

@api_controller.route('events/get-all-applicant/<int:event_id>',methods = ['GET'])
@authMiddleware
def getAllApplicantsEventById(current_user,event_id):
    db = get_db()
    records = db.execute('''
    select u.id as user_id,u.name,u.email,
    u.phone_number,a.cover_letter,
    a.status as applicant_status,a.created_at,
    a.event_id,a.id as applicant_id,
    m.url,m.size,m.name as fileName,ud.city,
    ud.country,ud.about,ud.skills,ud.birthday
    from users u 
    join applicants a on u.id = a.volunteer_id 
    join medias m on m.id = a.cv
    join user_detail ud on ud.user_id = u.id
    where a.event_id =?; 
    ''',(event_id,)).fetchall()
    result = []
    if(len(records)):
        for row in records:
            result.append(dict(row))
        return jsonify({"success":True,"data": result,"message":"Record Found"}),200
    else:
        return jsonify({"success":False,"message":"No Record Found"}),200

@api_controller.route('events/get-all-event-by-id/<int:event_id>',methods = ['GET'])
@authMiddleware
def getAllEventById(current_user,event_id):
    db = get_db()
    records = db.execute('''SELECT 
                            e.id,e.title,e.description,e.website,e.start_date,
                            e.end_date,e.organization_id,u.name as organization_name,e.status,e.max_application,
                            e.created_at,
                            e.duration,e.emptype,e.qualification,e.experience,e.location,e.isUrgent,
                            m.file as image,m.name,m.id as media_id
                            FROM events AS e 
                            JOIN medias m ON m.id = e.media_id
                            JOIN users u ON e.organization_id = u.id
                            where e.id = ?''',(event_id,)).fetchone()
    if(records):
        return jsonify({"success":True,"data": dict(records),"message":"Record Found"}),200
    return jsonify({"success":False,"message":"No Record Found"}),200

@api_controller.route('events/apply-job-application',methods = ['POST'])
@authMiddleware
def applyJobApplication(current_user):
    db = get_db()
    body = request.get_json()
    if(current_user['user_id'] != body['volunteer_id']):
            return jsonify({"success":False,"message":"UnAuthorized"}),401
    eventInfo = db.execute('SELECT id from events where id = ?',(body['event_id'],)).fetchone()
    if (eventInfo is None):
        return jsonify({"success":False,"message":"No Job Exist"}),200
    else:
        c = db.cursor()
        c.execute('BEGIN')
        try:
            #check you applied for this job or not
            record = c.execute('SELECT id from applicants where volunteer_id = ? and event_id = ?',(body['volunteer_id'], body['event_id'])).fetchone()
            if(record is None):
                c.execute('INSERT INTO medias (name,size,url,created_by,created_by) VALUES (?,?,?,?,?)', 
                        (body['fileName'], body['size'], body['cv'],current_user['user_id'],current_user['user_id']))
                c.execute('INSERT INTO applicants (volunteer_id,event_id,cover_letter,cv) VALUES (?,?,?,?)', 
                        (body['volunteer_id'], body['event_id'], body['cover_letter'],c.lastrowid))
                db.commit()
                return jsonify({"success":True,"message":"You successfully apply the job"}),200
            else:
                db.rollback()
                os.remove(os.path.join(body['cv'], body['fileName']))
                return jsonify({"success":False,"message":"You already apply this job"}),200
        except Exception as error:
            c.execute('ROLLBACK')
            os.remove(os.path.join(body['cv'], body['fileName']))
            return jsonify({"success":False,"message":str(error),"exception":traceback.format_exc()}),500


@api_controller.route('events/view-user-application',methods = ['GET'])
@authMiddleware
def viewApplication(current_user):
    db = get_db()
    records = db.execute('''
                  SELECT e.id,e.title,e.organization_id,e.description,e.status,e.emptype,e.created_at,a.status as applicantStatus,r.review from applicants 
                  AS a JOIN events e ON e.id = a.event_id
                  LEFT JOIN reviews r ON r.eventId = e.id
                  where a.volunteer_id = ?
                ''',(current_user['user_id'],)).fetchall()
    result = []
    if(len(records)):
        for row in records:
            result.append(dict(row))
        return jsonify({"success":True,"data": result,"message":"Record Found"}),200
    return jsonify({"success":False,"message":"No Record Found"}),200



@api_controller.route('users/user-info',methods = ['GET'])
@authMiddleware
def getUserInfo(current_user):
    db = get_db()
    records = db.execute('''SELECT 
                            u.id as userId,
                            u.name,
                            u.email,
                            u.phone_number,
                            u.role_id,
                            ud.city,
                            ud.country,
                            ud.about,
                            ud.skills,
                            ud.birthday,
                            '/'||m.url||'/'||m.name as profile_picture
                            FROM users u
                            LEFT JOIN user_detail ud ON u.id = ud.user_id
                            LEFT JOIN medias m ON m.id = u.profile_picture
                            where u.id = ?''',(current_user['user_id'],)).fetchone()
    if(records):
        return jsonify({"success":True,"data": dict(records),"message":"Record Found"}),200
    return jsonify({"success":False,"message":"No Record Found"}),200

@api_controller.route('users/update-user-info',methods = ['POST'])
@authMiddleware
def updateUserInfo(current_user):
    db = get_db()
    c = db.cursor()
    c.execute('BEGIN')
    body = request.get_json()
    userInfo =c.execute('SELECT user_id from user_detail where user_id = ?',(current_user['user_id'],)).fetchone()
    if(userInfo):
        try:
            c.execute('''
                        update user_detail SET
                        city = ?,
                        country = ?,
                        about = ?,
                        skills = ?,
                        birthday = ?
                        WHERE user_id = ?''',(body['city'],body['country'],body['about'],body['skills'],body['dob'],current_user['user_id'],))
            c.execute(''' 
                        update users SET
                        name = ?
                        WHERE id = ?''',(body['name'],current_user['user_id'],))
            db.commit()
            return jsonify({"success":True,"message":"User Profile Updated Sucessfully"}),200
        except Exception as error:
            c.execute('ROLLBACK')
            return jsonify({"success":False,"message":str(error),"exception":traceback.format_exc()}),500
    else:
        return jsonify({"success":False,"message":"User Not Found"}),200


@api_controller.route('applicant/change-applicant-status/<int:applicant_id>/<string:status>')
@authMiddleware
def changeStatus(current_user,applicant_id,status):
    db = get_db()
    if status not in ['pending','approved','rejected']:
        return jsonify({"success":False,"message":"Applicant Status doesnot Exists"}),200
    db.execute('''
         UPDATE applicants 
         SET status = ?
         WHERE id = ?
    ''',(status,applicant_id,))
    db.commit()
    return jsonify({"success":True,"message":"Applicant Status Updated Successfully"}),200

@api_controller.route('chat/get-all-user-chat/<int:sender_id>/<int:reciever_id>')
@authMiddleware
def getAllChats(current_user,sender_id,reciever_id):
    db = get_db()
    if(current_user['user_id'] !=sender_id):
        return jsonify({"success":False,"message":"UnAuthorized"}),401
    records = db.execute('''SELECT 
                            c.message,
                            s.id as senderId,
                            s.name as senderName,
                            sm.url as senderProfile,
                            sm.url as recieverProfile,
                            r.id as recieverId,
                            r.name as recieverName,
                            c.created_at
                            FROM chat AS c
                            JOIN users s ON s.id = c.sender_id
                            JOIN users r ON r.id = c.reciever_id
                            LEFT JOIN medias sm ON sm.id = s.profile_picture
                            LEFT JOIN medias rm on rm.id = r.profile_picture
                            where (c.sender_id = ? OR c.reciever_id = ?) AND (c.sender_id = ? OR c.reciever_id = ?) ''',(current_user['user_id'],current_user['user_id'],reciever_id,reciever_id)).fetchall()
    result = []
    if(len(records)):
        for row in records:
            result.append(dict(row))
        return jsonify({"success":True,"data": result,"message":"Record Found"}),200
    return jsonify({"success":False,"message":"No Record Found"}),200

@api_controller.route('chat/send-message',methods = ['POST'])
@authMiddleware
def sendMessage(current_user):
    db = get_db()
    body = request.get_json()
    if(current_user['user_id'] !=body['sender_id']):
        return jsonify({"success":False,"message":"UnAuthorized"}),401
    c = db.cursor()
    c.execute('''
    INSERT INTO chat (sender_id,reciever_id,message) VALUES
    (?,?,?)
    ''',(body['sender_id'],body['receiver_id'],body['message']))
    db.commit()
    if(c.lastrowid):
        return jsonify({"success":True,"message":"Message Send Sucessfully"}),200
    else:
        return jsonify({"success":False,"message":"Something went wrong"}),200

@api_controller.route('user/get-all-users')
@authMiddleware
def getAllUsers(current_user):
    db = get_db()
    records = db.execute('''
    SELECT u.id,u.name,u.email,u.phone_number,u.isBlocked,
    u.created_at,r.name as roleName,r.id as roleId,u.isSystemManager,
    u.isVolunteer,u.isOrganization 
    from users u
    JOIN roles r on r.id = u.role_id
    where u.id !=? and u.isDeleted is not true and r.id!=3
    ''',(current_user['user_id'],)).fetchall()
    result = []
    if(len(records)):
        for row in records:
            result.append(dict(row))
        return jsonify({"success":True,"data": result,"message":"Record Found"}),200
    return jsonify({"success":False,"message":"No Record Found"}),200

@api_controller.route('user/block-user',methods=["POST"])
@authMiddleware
def blockUser(current_user):
    db = get_db()
    raw_data = request.get_data()
    # Decode the byte string into a string
    data_str = raw_data.decode('utf-8')

    # Parse the string into a dictionary
    body = parse_qs(data_str)
    records = db.execute('''
    UPDATE users SET isBlocked = ?
    where id = ? and isDeleted is not true
    ''',(int(body['isBlocked'][0]),body['user_id'][0])).fetchall()
    db.commit()
    block = "Blocked" if body['isBlocked'] else "UnBlock" 
    return jsonify({"success":True,"message":"User %s Sucessfully"%block}),200

@api_controller.route('user/mark-as-system-manager',methods=["POST"])
@authMiddleware
def markAsSystemManager(current_user):
    db = get_db()
    raw_data = request.get_data()
    # Decode the byte string into a string
    data_str = raw_data.decode('utf-8')

    # Parse the string into a dictionary
    body = parse_qs(data_str)
    if(current_user['role_id']!=3):
        return jsonify({"success":False,"message":"only system manager perform this acrivity"}),200
    records = db.execute('''
    UPDATE users SET isSystemManager = ?
    where id = ? and isDeleted is not true
    ''',(int(body['isSystemManager'][0]),body['user_id'][0])).fetchall()
    db.commit()
    return jsonify({"success":True,"message":"Update User Sucessfully"}),200

@api_controller.route('user/mark-as-system-organizer',methods=["POST"])
@authMiddleware
def markAsOrangizer(current_user):
    db = get_db()
    raw_data = request.get_data()
    # Decode the byte string into a string
    data_str = raw_data.decode('utf-8')

    # Parse the string into a dictionary
    body = parse_qs(data_str)
    records = db.execute('''
    UPDATE users SET isOrganization = ?
    where id = ? and isDeleted is not true
    ''',(int(body['isOrganization'][0]),body['user_id'][0])).fetchall()
    db.commit()
    return jsonify({"success":True,"message":"Update User Sucessfully"}),200

@api_controller.route('user/mark-as-volunteer',methods=["POST"])
@authMiddleware
def markAsVolunteer(current_user):
    db = get_db()
    raw_data = request.get_data()
    # Decode the byte string into a string
    data_str = raw_data.decode('utf-8')

    # Parse the string into a dictionary
    body = parse_qs(data_str)
    records = db.execute('''
    UPDATE users SET isVolunteer = ?
    where id = ? and isDeleted is not true
    ''',(int(body['isVolunteer'][0]),body['user_id'][0])).fetchall()
    db.commit()
    return jsonify({"success":True,"message":"Update User Sucessfully"}),200

@api_controller.route('dashboard/get-all-summary')
@authMiddleware
def getAllSummary(current_user):
    db = get_db()
    record = db.execute('''
    SELECT
    (SELECT count(id)  FROM users WHERE created_at BETWEEN datetime('now', '-6 days') AND datetime('now', 'localtime')) as totalUsers,
    (SELECT count(id) FROM events WHERE created_at BETWEEN datetime('now', '-6 days') AND datetime('now', 'localtime')) as totalJobPosts
    ''').fetchone()
    return jsonify({"success":True,"message":"Record Found","data":dict(record)}),200

@api_controller.route('user/user-profile-photo',methods=['GET', 'POST'])
@authMiddleware
def userProfilePhoto(current_user):
    file = request.files['file']
    image_data = file.read()
    filename = secure_filename(file.filename)  # sanitize the filename
    filePath = 'assets/profile/user-'+str(current_user['user_id'])
    db = get_db()
    c = db.cursor()
    c.execute('BEGIN')
    try:
        user = c.execute('SELECT id,profile_picture from users where id = ?',(current_user['user_id'],)).fetchone()
        if(user and user['profile_picture']):
            c.execute('UPDATE medias set name = ? , size = ? , url = ? where id = ?',(filename,len(image_data), filePath,user['profile_picture']))
        else:
            c.execute('INSERT INTO medias (name,size,url,created_by,created_by) VALUES (?,?,?,?,?)', 
                        (filename,len(image_data), filePath,current_user['user_id'],current_user['user_id']))
            c.execute('UPDATE users SET profile_picture = ? where id = ? ',(c.lastrowid,current_user['user_id']))
        db.commit()
        os.makedirs(filePath, exist_ok=True)
        file.seek(0)
        file.save(os.path.join(filePath, filename))
        return jsonify({"success":True,"message":"Profile Updated Sucessfully"}),200
    except Exception as error:
            c.execute('ROLLBACK')
            os.remove(os.path.join(filePath, filename))
            return jsonify({"success":False,"message":str(error),"exception":traceback.format_exc()}),500

@api_controller.route('contact/contact-us',methods=['POST'])
def contactUs():
    db = get_db()
    body = request.get_json()
    db.execute('''
    INSERT INTO contact (name,email,subject,comment)
    VALUES (?,?,?,?) 
    ''',(body['name'],body['email'],body['subject'],body['comment']))
    db.commit()
    return jsonify({"success":True,"message":"Message Save Sucessfully"}),200
@api_controller.route('review/add-review',methods=['POST'])
@authMiddleware
def addReview(current_user):
    db = get_db()
    body = request.get_json()
    review = db.execute("select * from reviews where user_id=? and eventId = ?",(current_user['user_id'],body['event_id'])).fetchone()
    if(review):
        return jsonify({"success":False,"message":"You Already Added review for this event"}),200
    db.execute('''
    INSERT INTO reviews (review,rating,user_id,eventId)
    VALUES (?,?,?,?) 
    ''',(body['review'],body['rating'],current_user['user_id'],body['event_id']))
    db.commit()
    return jsonify({"success":True,"message":"Review Added Sucessfully"}),200

@api_controller.route('review/get-all-review/<int:event_id>',methods=['GET'])
@authMiddleware
def getAllReviews(current_user,event_id):
    db = get_db()
    records = db.execute('''
     SELECT r.id,r.review,r.rating,ru.name as userName,eu.name as organizationName,r.created_at FROM reviews as r 
    JOIN events as e ON r.eventId = e.id
    JOIN users as ru ON ru.id = r.user_id
    JOIN users as eu ON eu.id = e.organization_id
    WHERE e.id = ?
    ''',(event_id,)).fetchall()
    result = []
    if(len(records)):
        for row in records:
            result.append(dict(row))
        return jsonify({"success":True,"data": result,"message":"Record Found"}),200
    return jsonify({"success":False,"message":"No Record Found"}),200

@api_controller.route('contact/get-all-contact',methods=['GET'])
@authMiddleware
def getAllContact(current_user):
    db = get_db()
    records = db.execute('''
     SELECT * from contact
    ''').fetchall()
    result = []
    if(len(records)):
        for row in records:
            result.append(dict(row))
        return jsonify({"success":True,"data": result,"message":"Record Found"}),200
    return jsonify({"success":False,"message":"No Record Found"}),200

@api_controller.route('chart/get-all-users-created_at',methods=['GET'])
@authMiddleware
def getAllUsersCreatedAt(current_user):
    db = get_db()
    records = db.execute('''
     select count(id) as registerUser,created_at from users group by created_at
    ''').fetchall()
    result = []
    if(len(records)):
        for row in records:
            result.append(dict(row))
        return jsonify({"success":True,"data": result,"message":"Record Found"}),200
    return jsonify({"success":False,"message":"No Record Found"}),200

@api_controller.route('chart/get-all-jobs-created_at',methods=['GET'])
@authMiddleware
def getAllJobsCreatedAt(current_user):
    db = get_db()
    records = db.execute('''
     select count(id) as registerJobs,created_at from events group by created_at
    ''').fetchall()
    result = []
    if(len(records)):
        for row in records:
            result.append(dict(row))
        return jsonify({"success":True,"data": result,"message":"Record Found"}),200
    return jsonify({"success":False,"message":"No Record Found"}),200

@api_controller.errorhandler(Exception)
def handle_error(error):
    # Create a JSON response with a custom error message
    return jsonify({"success":False,"message":str(error),"exception":traceback.format_exc()}),500