import sqlite3
from flask import g
from config import DATABASE_URL

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE_URL)
        db.row_factory = sqlite3.Row
    return db