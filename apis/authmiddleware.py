from flask import request,jsonify
from functools import wraps
import jwt
import traceback
from config import JWT_SECRET_KEY
import sqlite3
from flask import g
from config import DATABASE_URL

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE_URL)
        db.row_factory = sqlite3.Row
    return db
def authMiddleware(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None
        if "Authorization" in request.headers:
            token = request.headers["Authorization"].split(" ")[1]
        if not token:
            return {
                "success": False,
                "message": "Unauthorized"
            }, 401
        data=jwt.decode(token, JWT_SECRET_KEY, algorithms=["HS256"])
        db = get_db()
        current_user = db.execute('SELECT id as user_id FROM users WHERE id = ?',(data['user_id'],)).fetchone()
        if current_user is None:
            return {
                "success": False,
                "message": "Unauthorized",
        }, 401
        return f(current_user, *args, **kwargs)
    return decorated
