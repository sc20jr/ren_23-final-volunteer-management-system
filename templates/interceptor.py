from functools import wraps
from flask import session,redirect,request
import json
def authInterceptor(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        # if(session.get('access_token') and (request.path in '/login' or request.path in '/sign-up')):
        #     return redirect("/volunteer-tasks")
        if(session.get('access_token') is None):
            return redirect("/login")
        return f(session.get('access_token'), *args, **kwargs)
    return decorated
def protected_guard(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        user =  session.get('user')
        user = json.loads(user) if user else None
        if(user and user['isBlocked']==1):
            return redirect('/contact-us')
        if(user and (user['role_id'] != 1 or user['isVolunteer']!=0)):
            return redirect('/home')
        return f(*args, **kwargs)
    return decorated

def protected_guard_admin(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        user =  session.get('user')
        user = json.loads(user) if user else None
        #if volunteer is not admin and not organizer
        if(user and user['role_id'] ==1 and user['isSystemManager']==0 and user['isOrganization']==0 ):
            return redirect('/home')
        #if organization is not a system manager and vlounteer is organizer
        if((request.path == '/dashboard' or request.path == '/user') and user and (user['role_id']==2 or user['isOrganization']==1) and user['isSystemManager']==0):
            return redirect('/volunteer-task')
        
        return f(*args, **kwargs)
    return decorated