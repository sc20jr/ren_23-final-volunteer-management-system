from flask import Blueprint,render_template,redirect,session,request
from templates.interceptor import authInterceptor,protected_guard_admin
from config import apiUrl
from flask import flash
import json
admin_blueprint = Blueprint('admin_blueprint', __name__)
from templates.authentication.forms.loginform import PostJob,SendMessage
import base64
import requests
from wtforms import  validators
from datetime import datetime
from config import documentUrl
@admin_blueprint.route('/dashboard')
@protected_guard_admin
@authInterceptor
def dashboard(access_token):
    user =  session.get('user')
    user = json.loads(user) if user else None
    resp = requests.get(
            apiUrl+'/dashboard/get-all-summary',
            headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Bearer " + access_token
            })
    resp = json.loads(resp.text)
    data = resp['data'] if(resp['success']) else None
    #get user group by
    resp = requests.get(
            apiUrl+'/chart/get-all-users-created_at',
            headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Bearer " + access_token
            })
    resp = json.loads(resp.text)
    chartUser = resp['data'] if(resp['success']) else None
    #get jobs group by
    resp = requests.get(
            apiUrl+'/chart/get-all-jobs-created_at',
            headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Bearer " + access_token
            })
    resp = json.loads(resp.text)
    chartJob = resp['data'] if(resp['success']) else None
    return render_template('admin/views/dashboard.html',page='dashboard',user = user,summary=data,access_token=access_token,chartUser=chartUser,chartJob=chartJob)
    
#
# @admin_blueprint.route('/sign-in',methods=['GET', 'POST'])
# def signIn():
#   login_form = LoginForm(request.form)
#   return render_template('admin/views/sign-in.html',form=login_form)

# @admin_blueprint.route('/sign-up-volunteer',methods=['GET', 'POST'])
# def signUpVolunteer():
#     create_account_form = CreateAccountForm(request.form)
#     return render_template('admin/views/sign-up-volunteer.html',form=create_account_form)
#
# @admin_blueprint.route('/sign-up-enterprise',methods=['GET', 'POST'])
# def signUpEnterprise():
#  create_account_form = CreateAccountForm(request.form)
#  return render_template('admin/views/sign-up-enterprise.html',form=create_account_form)

@admin_blueprint.route('/volunteer-task')
@protected_guard_admin
@authInterceptor
def volunteerTask(access_token):
    user =  session.get('user')
    user = json.loads(user) if user else None
    body = {
        "page":1,
        "limit":10,
        "filter": request.args['filter'] if request.args and request.args['filter'] else ''
    }
    resp = requests.post(
            apiUrl+'/events/get-all-events-by-organization',
            json.dumps(body, indent = 4),
            headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Bearer " + access_token
            })
    resp = json.loads(resp.text)
    if(resp['success']):
        return render_template('admin/views/volunteer-task.html',events=resp['data'],page="events",user=user,access_token=access_token)
    else:
        flash(resp['message'],'error')
        return render_template('admin/views/volunteer-task.html',events=[],page="events",user=user,access_token=access_token)

@admin_blueprint.route('/add-update-volunteer-task',methods=['GET','POST'])
@protected_guard_admin
@authInterceptor
def addUpdateVolunteerTask(access_token):
    user =  session.get('user')
    user = json.loads(user) if user else None
    form =  PostJob()
    # data = request.args.get('data')
    # if(data is not None):
    #     data = ast.literal_eval()
    #     form.title.data = data['title']
    #     form.description.data = data['description']
    #     form.max_application.data = data['max_application']
    #     return render_template('admin/views/add-update-volunteer-task.html',form=form,resp=None)

    if form.validate_on_submit():
        file = dict(request.files)['image']
        if(file.filename==''):
            flash("File is required", 'error')
            return render_template('admin/views/add-update-volunteer-task.html',form=form,resp=None,user=user,access_token=access_token)
        image_data = file.read()
        image_base64 = base64.b64encode(image_data).decode('utf-8')
        body = {
            "title": form.title.data,
            "description":form.description.data,
            "website":form.website.data,
            "start_date":form.start_date.data.isoformat(),
            "end_date":form.end_date.data.isoformat(),
            "max_application":form.max_application.data,
            "duration":form.duration.data,
            "isUrgent":form.isUrgent.data,
            "emptype":form.job_type.data,
            "qualification":form.qualification.data,
            "experience":form.experience.data,
            "location":form.location.data,
            "organization_id":json.loads(session.get('user'))['user_id'],
            "image":image_base64,
            "fileName":file.filename,
            "size": len(image_data)
        }
        resp = requests.post(
            apiUrl+'/events/add-event',
            json.dumps(body, indent = 4),
            headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Bearer " + access_token
            })
        resp = json.loads(resp.text)
        if(resp['success']):
            #reset form
            form.process()
            flash(resp['message'], 'success')
            # return render_template('admin/views/add-update-volunteer-task.html',form=form,resp=resp)
            return redirect('/volunteer-task')
        else:
            flash(resp['message'], 'error')
            return render_template('admin/views/add-update-volunteer-task.html',form=form,resp=resp,user=user,access_token=access_token)
    return render_template('admin/views/add-update-volunteer-task.html',form=form,resp=None,user=user,access_token=access_token)

@admin_blueprint.route('/edit-event/<int:event_id>',methods=['GET','POST'])
@protected_guard_admin
@authInterceptor
def editEvent(access_token,event_id):
    user =  session.get('user')
    user = json.loads(user) if user else None
    form =  PostJob()
    resp = requests.get(
            apiUrl+'/events/get-all-event-by-id/'+str(event_id),
            headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Bearer " + access_token
            })
    resp = json.loads(resp.text)
    if(resp['success'] and request.method == "GET"):
        form.title.data = resp['data']['title']
        form.description.data = resp['data']['description']
        form.website.data = resp['data']['website']
        form.start_date.data = datetime.strptime(resp['data']['start_date'], '%Y-%m-%d').date()
        form.end_date.data = datetime.strptime(resp['data']['end_date'], '%Y-%m-%d').date()
        form.max_application.data = resp['data']['max_application']
        form.duration.data = resp['data']['max_application']
        form.job_type.data = resp['data']['emptype']
        form.qualification.data = resp['data']['qualification']
        form.experience.data = resp['data']['experience']
        form.location.data = resp['data']['location']
        form.status.data = resp['data']['status']
        form.isUrgent.data = resp['data']['isUrgent']
    # form.image.data = resp['data']['name']
        return render_template('admin/views/edit-job.html',form=form,resp=resp,user=user,access_token=access_token)
    else:
        file = dict(request.files)['image']
        image_base64 = None
        image2 = None
        image_data = None
        if(file):
            image_data = file.read()
            image_base64 = base64.b64encode(image_data).decode('utf-8')
        else:
            image2 = resp['data']['image']
        body = {
            "title": form.title.data,
            "description":form.description.data,
            "website":form.website.data,
            "start_date":form.start_date.data.isoformat(),
            "end_date":form.end_date.data.isoformat(),
            "max_application":form.max_application.data,
            "duration":form.duration.data,
            "isUrgent":form.isUrgent.data,
            "emptype":form.job_type.data,
            "qualification":form.qualification.data,
            "experience":form.experience.data,
            "location":form.location.data,
            "status":form.status.data,
            "organization_id":json.loads(session.get('user'))['user_id'],
            "event_id":resp['data']['id'],
            "media_id":resp['data']['media_id']
        }
        if(image2 is not None):
            body["old_image"] = image2
        else:
            body["image"] = image_base64
            body["name"] = file.filename
            body["size"] = len(image_data)

        newResp = requests.post(
            apiUrl+'/events/update-event',
            json.dumps(body, indent = 4),
            headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Bearer " + access_token
            })
        newResp = json.loads(newResp.text)
        if(newResp['success']):
            flash('Update Event Successfully', 'success')
            return render_template('admin/views/edit-job.html',form=form,resp=newResp,user=user,access_token=access_token)
        else:
            flash('Error on updating event', 'error')
            return render_template('admin/views/edit-job.html',form=form,resp=newResp,user=user,access_token=access_token)
        

@admin_blueprint.route('/delete-event/<int:organization_id>/<int:event_id>',methods =["GET"])
@protected_guard_admin
@authInterceptor
def deleteEvent(access_token,organization_id,event_id):
    body = {
        "organization_id":organization_id,
        "event_id":event_id
    }
    resp = requests.post(
            apiUrl+'/events/delete-event',
            json.dumps(body, indent = 4),
            headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Bearer " + access_token
            })
    resp = json.loads(resp.text)
    if(resp['success']):
        flash('Event Deleted Successfully', 'success')
        return redirect('/volunteer-task')
    else:
        flash('Error on deleting event', 'error')
        return redirect('/volunteer-task')
    

@admin_blueprint.route('/detail-event/<int:event_id>')
@protected_guard_admin
@authInterceptor
def detailEvent(access_token,event_id):
     user =  session.get('user')
     user = json.loads(user) if user else None
     resp = requests.get(
            apiUrl+'/events/get-all-event-by-id/'+str(event_id),
            headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Bearer " + access_token
            })
     resp = json.loads(resp.text)
     eventInfo = resp['data'] if resp['success'] else None
     resp = requests.get(
            apiUrl+'/events/get-all-applicant/'+str(event_id),
            headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Bearer " + access_token
            })
     resp = json.loads(resp.text)
     applicants= resp['data'] if resp['success'] else []
     resp = requests.get(
            apiUrl+'/review/get-all-review/'+str(event_id),
            headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Bearer " + access_token
            })
     resp = json.loads(resp.text)
     reviews= resp['data'] if resp['success'] else []
     return render_template('admin/views/detail-event.html',eventInfo=eventInfo,applicants=applicants,documentUrl=documentUrl,page='events',user=user,access_token=access_token,reviews=reviews)

@admin_blueprint.route('/change-applicant-status/<int:event_id>/<int:applicant_id>/<string:status>')
@protected_guard_admin
@authInterceptor
def changeStatus(access_token,event_id,applicant_id,status):
     resp = requests.get(
            apiUrl+'/applicant/change-applicant-status/'+str(applicant_id)+'/'+status,
            headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Bearer " + access_token
            })
     resp = json.loads(resp.text)
     if resp['success']:
         flash(resp['message'],'success')
     else:
         flash(resp['message'],'error')
     return redirect('/detail-event/'+str(event_id))

# @admin_blueprint.route('/send-message/<int:sender_id>/<int:reciever_id>')
# @authInterceptor
# def sendMessage(access_token,sender_id,reciever_id):
#     return render_template('admin/views/chat.html')


@admin_blueprint.route('/admin/chat/<int:receiver_id>')
@protected_guard_admin
@authInterceptor
def adminChat(access_token,receiver_id):
    user =  session.get('user')
    user = json.loads(user) if user else None
    userId = json.loads(session.get('user'))['user_id']
    form = SendMessage()
    resp = requests.get(
            apiUrl+'/chat/get-all-user-chat/'+str(json.loads(session.get('user'))['user_id'])+'/'+str(receiver_id),
            headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Bearer " + access_token
            })
    resp = json.loads(resp.text)
    data = resp['data'] if resp['success'] else []
    return render_template('admin/views/chat.html',chats=data,userId=userId,receiver_id=receiver_id,form=form,user=user,access_token=access_token)

@admin_blueprint.route('/admin/send-message/<int:reciever_id>',methods=['GET','POST'])
@protected_guard_admin
@authInterceptor
def sendMessage(access_token,reciever_id):
    form = SendMessage()
    body = {
        "sender_id":json.loads(session.get('user'))['user_id'],
        "receiver_id":reciever_id,
        "message":form.message.data
    }
    resp = requests.post(
            apiUrl+'/chat/send-message',
            json.dumps(body, indent = 4),
            headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Bearer " + access_token
            })
    resp = json.loads(resp.text)
    if(resp['success']):
        return redirect('/admin/chat/'+str(reciever_id))
    else:
        flash('Error on sending message','error')
        return redirect('/admin/chat/'+str(reciever_id))

# user CRUD

@admin_blueprint.route('/user')
@protected_guard_admin
@authInterceptor
def user(access_token):
    # body = {
    #     "page":1,
    #     "limit":10,
    #     "filter": request.args['filter'] if request.args and request.args['filter'] else ''
    # }
    user =  session.get('user')
    user = json.loads(user) if user else None
    resp = requests.get(
            apiUrl+'/user/get-all-users',
            # json.dumps(body, indent = 4),
            headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Bearer " + access_token
            })
    resp = json.loads(resp.text)
    data = resp['data'] if resp['success'] else []
    return render_template('admin/views/user.html',events=data,page="user",access_token=access_token,user=user)

# contact CRUD

@admin_blueprint.route('/contact')
@protected_guard_admin
@authInterceptor
def contact(access_token):
    # body = {
    #     "page":1,
    #     "limit":10,
    #     "filter": request.args['filter'] if request.args and request.args['filter'] else ''
    # }
    user =  session.get('user')
    user = json.loads(user) if user else None
    resp = requests.get(
            apiUrl+'/contact/get-all-contact',
            # json.dumps(body, indent = 4),
            headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Bearer " + access_token
            })
    resp = json.loads(resp.text)
    data = resp['data'] if resp['success'] else []
    return render_template('admin/views/contact.html',events=data,page="contact",access_token=access_token,user=user)

# reviews CRUD

@admin_blueprint.route('/reviews')
@protected_guard_admin
@authInterceptor
def reviews(access_token):
    # body = {
    #     "page":1,
    #     "limit":10,
    #     "filter": request.args['filter'] if request.args and request.args['filter'] else ''
    # }
    user =  session.get('user')
    user = json.loads(user) if user else None
    resp = requests.get(
            apiUrl+'/user/get-all-users',
            # json.dumps(body, indent = 4),
            headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Bearer " + access_token
            })
    resp = json.loads(resp.text)
    data = resp['data'] if resp['success'] else []
    return render_template('admin/views/reviews.html',events=data,page="reviews",access_token=access_token,user=user)

# @admin_blueprint.route('/add-user',methods=['GET','POST'])
# @authInterceptor
# def addUser(access_token):
#     form =  PostJob()
#     # data = request.args.get('data')
#     # if(data is not None):
#     #     data = ast.literal_eval()
#     #     form.title.data = data['title']
#     #     form.description.data = data['description']
#     #     form.max_application.data = data['max_application']
#     #     return render_template('admin/views/add-update-volunteer-task.html',form=form,resp=None)

#     if form.validate_on_submit():
#         file = dict(request.files)['image']
#         if(file.filename==''):
#             flash("File is required", 'error')
#             return render_template('admin/views/add-user.html',form=form,resp=None)
#         image_data = file.read()
#         image_base64 = base64.b64encode(image_data).decode('utf-8')
#         body = {
#             "title": form.title.data,
#             "description":form.description.data,
#             "start_date":form.start_date.data.isoformat(),
#             "end_date":form.end_date.data.isoformat(),
#             "max_application":form.max_application.data,
#             "duration":form.duration.data,
#             "isUrgent":form.isUrgent.data,
#             "emptype":form.job_type.data,
#             "qualification":form.qualification.data,
#             "experience":form.experience.data,
#             "location":form.location.data,
#             "organization_id":json.loads(session.get('user'))['user_id'],
#             "image":image_base64,
#             "fileName":file.filename,
#             "size": len(image_data)
#         }
#         resp = requests.post(
#             apiUrl+'/events/add-event',
#             json.dumps(body, indent = 4),
#             headers = {
#             "Content-Type": "application/json; charset=utf-8",
#             "Authorization": "Bearer " + access_token
#             })
#         resp = json.loads(resp.text)
#         if(resp['success']):
#             #reset form
#             form.process()
#             flash(resp['message'], 'success')
#             # return render_template('admin/views/add-update-volunteer-task.html',form=form,resp=resp)
#             return redirect('/volunteer-task')
#         else:
#             flash(resp['message'], 'error')
#             return render_template('admin/views/add-user.html',form=form,resp=resp)
#     return render_template('admin/views/add-user.html',form=form,resp=None)

# @admin_blueprint.route('/edit-user/<int:event_id>',methods=['GET','POST'])
# @authInterceptor
# def editUser(access_token,event_id):
#     form =  PostJob()
#     resp = requests.get(
#             apiUrl+'/events/get-all-event-by-id/'+str(event_id),
#             headers = {
#             "Content-Type": "application/json; charset=utf-8",
#             "Authorization": "Bearer " + access_token
#             })
#     resp = json.loads(resp.text)
#     if(resp['success'] and request.method == "GET"):
#         form.title.data = resp['data']['title']
#         form.description.data = resp['data']['description']
#         form.start_date.data = datetime.strptime(resp['data']['start_date'], '%Y-%m-%d').date()
#         form.end_date.data = datetime.strptime(resp['data']['end_date'], '%Y-%m-%d').date()
#         form.max_application.data = resp['data']['max_application']
#         form.duration.data = resp['data']['max_application']
#         form.job_type.data = resp['data']['emptype']
#         form.qualification.data = resp['data']['qualification']
#         form.experience.data = resp['data']['experience']
#         form.location.data = resp['data']['location']
#         form.status.data = resp['data']['status']
#         form.isUrgent.data = resp['data']['isUrgent']
#     # form.image.data = resp['data']['name']
#         return render_template('admin/views/edit-user.html',form=form,resp=resp)
#     else:
#         file = dict(request.files)['image']
#         image_base64 = None
#         image2 = None
#         image_data = None
#         if(file):
#             image_data = file.read()
#             image_base64 = base64.b64encode(image_data).decode('utf-8')
#         else:
#             image2 = resp['data']['image']
#         body = {
#             "title": form.title.data,
#             "description":form.description.data,
#             "start_date":form.start_date.data.isoformat(),
#             "end_date":form.end_date.data.isoformat(),
#             "max_application":form.max_application.data,
#             "duration":form.duration.data,
#             "isUrgent":form.isUrgent.data,
#             "emptype":form.job_type.data,
#             "qualification":form.qualification.data,
#             "experience":form.experience.data,
#             "location":form.location.data,
#             "status":form.status.data,
#             "organization_id":json.loads(session.get('user'))['user_id'],
#             "event_id":resp['data']['id'],
#             "media_id":resp['data']['media_id']
#         }
#         if(image2 is not None):
#             body["old_image"] = image2
#         else:
#             body["image"] = image_base64
#             body["name"] = file.filename
#             body["size"] = len(image_data)

#         newResp = requests.post(
#             apiUrl+'/events/update-event',
#             json.dumps(body, indent = 4),
#             headers = {
#             "Content-Type": "application/json; charset=utf-8",
#             "Authorization": "Bearer " + access_token
#             })
#         newResp = json.loads(newResp.text)
#         if(newResp['success']):
#             flash('Update Event Successfully', 'success')
#             return render_template('admin/views/edit-user.html',form=form,resp=newResp)
#         else:
#             flash('Error on updating event', 'error')
#             return render_template('admin/views/edit-user.html',form=form,resp=newResp)
        

# @admin_blueprint.route('/delete-user/<int:organization_id>/<int:event_id>',methods =["GET"])
# @authInterceptor
# def deleteUser(access_token,organization_id,event_id):
#     body = {
#         "organization_id":organization_id,
#         "event_id":event_id
#     }
#     resp = requests.post(
#             apiUrl+'/events/delete-event',
#             json.dumps(body, indent = 4),
#             headers = {
#             "Content-Type": "application/json; charset=utf-8",
#             "Authorization": "Bearer " + access_token
#             })
#     resp = json.loads(resp.text)
#     if(resp['success']):
#         flash('User Deleted Successfully', 'success')
#         return redirect('/volunteer-task')
#     else:
#         flash('Error on deleting user', 'error')
#         return redirect('/volunteer-task')
    
