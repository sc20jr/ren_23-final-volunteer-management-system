from flask import Blueprint,render_template,redirect,request,session
import requests
import json
from templates.authentication.forms.loginform import LoginForm,SignUpForm
from config import apiUrl
from flask import flash
from templates.interceptor import authInterceptor
auth_blueprint = Blueprint('auth_blueprint', __name__)

@auth_blueprint.route('/login',methods=['GET','POST'])
# @authInterceptor
def login():
    form = LoginForm()
    if form.validate_on_submit():
        body = {
            "email": request.form['email'],
            "password": request.form['password']
        }
        resp = requests.post(apiUrl+'/auth/authenticate',json.dumps(body, indent = 4),headers = {"Content-Type": "application/json; charset=utf-8"})
        resp = json.loads(resp.text)
        if(resp['success'] and resp['data']['userInfo']['isBlocked'] ==1):
            session['access_token'] = resp['data']['access_token']
            session['user'] = json.dumps(resp['data']['userInfo'], indent=4)
            return redirect('/contact-us')
        if(resp['success'] and (resp['data']['userInfo']['role_id']==2 or resp['data']['userInfo']['role_id']==3)):
            session['access_token'] = resp['data']['access_token']
            session['user'] = json.dumps(resp['data']['userInfo'], indent=4)
            flash(resp['message'], 'success')
            if(resp['data']['userInfo']['role_id']==2 and resp['data']['userInfo']['isSystemManager'] == 0):
                return redirect('/volunteer-task')
            return redirect('/dashboard')
        elif(resp['success'] and resp['data']['userInfo']['role_id']==1):
            session['access_token'] = resp['data']['access_token']
            session['user'] = json.dumps(resp['data']['userInfo'], indent=4)
            flash(resp['message'], 'success')
            return redirect('/volunteer-tasks')
        else:
            flash(resp['message'], 'error')
            return render_template('authentication/views/login.html', form=form,resp=None)
    return render_template('authentication/views/login.html', form=form,resp=None)

@auth_blueprint.route('/sign-up',methods=['GET','POST'])
# @authInterceptor
def signUpVolunteer():
    form = SignUpForm()
    # if form.validate_on_submit():
    if form.validate_on_submit():
        body = {
            "name": request.form['name'],
            "email": request.form['email'],
            "phone_number":  request.form['phone_number'],
            "role_id":  request.form['role'],
            "password": request.form['password']
        }
        resp = requests.post(apiUrl+'/auth/sign-up',json.dumps(body, indent = 4),headers = {"Content-Type": "application/json; charset=utf-8"})
        resp = json.loads(resp.text)
        if(resp['success']):
            flash(resp['message'], 'success')
            return redirect('/login')
        else:
            flash(resp['message'], 'error')
            return render_template('authentication/views/sign-up-volunteer.html',form=form,resp=None)
    return render_template('authentication/views/sign-up-volunteer.html',form=form)

# @auth_blueprint.route('/sign-up-enterprise')
# def signUpEnterprise():
#     return render_template('authentication/views/sign-up-enterprise.html', segment='index')
@auth_blueprint.route('/logout')
def logout():
    session.clear()
    return redirect('/login')
