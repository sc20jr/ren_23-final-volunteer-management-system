# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from flask_wtf import FlaskForm
from wtforms import StringField,HiddenField, PasswordField,DateField,FileField,SelectField,TextAreaField,RadioField,SubmitField
from wtforms.validators import Email, DataRequired

# login and registration


class LoginForm(FlaskForm):
    email = StringField('Username',
                         id='username_login',
                         validators=[DataRequired()])
    password = PasswordField('Password',
                             id='pwd_login',
                             validators=[DataRequired()])


class SignUpForm(FlaskForm):
    name = StringField('Username',
                         id='username_create',
                         validators=[DataRequired()])
    email = StringField('Email',
                      id='email_create',
                      validators=[DataRequired()])
    phone_number = StringField('Phone',
                      id='phone_number',
                      validators=[DataRequired()])
    # , (3, 'System Manager')
    role = SelectField('Role',
                      choices=[(1, 'Volunteer'), (2, 'Organization')],
                      validators=[DataRequired()])
    password = PasswordField('Password',
                             id='pwd_create',
                             validators=[DataRequired()])
class PostJob(FlaskForm):
    title = StringField('tile',
                         id='title',
                         validators=[DataRequired()])
    description = StringField('description',
                         id='description',
                         validators=[DataRequired()])
    website = StringField('website',
                         id='website',
                         validators=[DataRequired()])
    start_date = DateField('start_date',
                        id='start_date',
                        validators=[DataRequired()])
    end_date = DateField('end_date',
                        id='end_date',
                        validators=[DataRequired()])
    max_application = StringField('max_application',
                        id='max_application',
                        validators=[DataRequired()])
    duration = StringField('duration',
                        id='duration',
                        validators=[DataRequired()])   
    job_type = StringField('job_type',
                        id='job_type',
                        validators=[DataRequired()])         
    qualification = StringField('qualification',
                        id='qualification',
                        validators=[DataRequired()])  
    experience = StringField('experience',
                        id='experience',
                        validators=[DataRequired()])
    location = StringField('location',
                        id='location',
                        validators=[DataRequired()])  
    status = SelectField('Status',
                      choices=[('requiring', 'Requiring'), ('processing', 'Processing'), ('end', 'End')], default='requiring')
    isUrgent = RadioField('isUrgent',
                        id='isUrgent',
                        choices=[(1,'Urgent'),(0,'Normal')],
                        validators=[DataRequired()])     
    image = FileField('image',
                        id='image')
    
class ApplyJob(FlaskForm):
    cover_letter = TextAreaField('cover_letter',
                        id='cover_letter',
                        validators=[DataRequired()])
    cv = FileField('cv',id='cv',validators=[DataRequired()])


class UserProfile(FlaskForm):
    name = StringField('name',
                        id='name',
                        validators=[DataRequired()])
    city = StringField('city',
                        id='city',
                        validators=[DataRequired()])
    country = StringField('country',
                        id='country',
                        validators=[DataRequired()])
    about = StringField('about',
                        id='about',
                        validators=[DataRequired()])
    skills = StringField('skills',
                        id='skills',
                        validators=[DataRequired()])
    dob = DateField('dob',
                        id='dob',
                        validators=[DataRequired()])

class SendMessage(FlaskForm):
    message = StringField('message',
                        id='message',
                        validators=[DataRequired()])
    


class ContactUs(FlaskForm):
    name = StringField('name',
                        id='name',
                        validators=[DataRequired()])
    email = StringField('email',
                        id='email',
                        validators=[DataRequired()])
    subject = StringField('subject',
                        id='subject',
                        validators=[DataRequired()])
    comment = TextAreaField('comment',
                        id='comment',
                        validators=[DataRequired()])
class ReviewForm(FlaskForm):
    rating = HiddenField('rating')
    review = TextAreaField('review', validators=[DataRequired()])