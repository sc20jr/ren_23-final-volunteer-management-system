from flask import Blueprint,render_template,redirect,request,session
import requests
from templates.interceptor import authInterceptor,protected_guard
import json
from config import apiUrl
from flask import flash
import base64
from datetime import datetime
from templates.authentication.forms.loginform import ApplyJob,UserProfile,SendMessage,ContactUs,ReviewForm
from werkzeug.utils import secure_filename
import os
website_blueprint = Blueprint('website_blueprint', __name__)
@website_blueprint.route('/')
def index():
    return redirect('/home')

@website_blueprint.route('/home')
def home():
    user =  session.get('user')
    user = json.loads(user) if user else None
    return render_template('website/views/home.html',user = user, segment='index')

@website_blueprint.route('/contact-us',methods = ['GET','POST'])
def contactUs():
    form = ContactUs()
    user =  session.get('user')
    user = json.loads(user) if user else None
    if(user and user['isBlocked']==1):
        form.name.data = user['name']
        form.email.data = user['email']
        form.name.render_kw = {'disabled': True}
        form.email.render_kw = {'disabled': True}
    if form.validate_on_submit():
        body = {
            "name":form.name.data,
            "email":form.email.data,
            "subject":form.subject.data,
            "comment":form.comment.data
        }
        resp = requests.post(
        apiUrl+'/contact/contact-us',
        json.dumps(body, indent = 4),
        headers = {
        "Content-Type": "application/json; charset=utf-8"
        })
        resp = json.loads(resp.text)
        form.process()
        if(resp['success']):
            flash('Message Send Successfully', 'success')
        else:
            flash('Error on sending message', 'error')
    return render_template('website/views/contact-us.html',user = user, segment='index',form=form)

@website_blueprint.route('/about-us')
def aboutUs():
    user =  session.get('user')
    user = json.loads(user) if user else None
    return render_template('website/views/about-us.html',user = user, segment='index')

@website_blueprint.route('/corporate-values')
def corporateValues():
    user =  session.get('user')
    user = json.loads(user) if user else None
    return render_template('website/views/corporate-values.html',user = user, segment='index')

@website_blueprint.route('/chat/<int:receiver_id>')
@protected_guard
@authInterceptor
def chat(access_token,receiver_id):
    user = json.loads(session.get('user'))
    userId = user['user_id']
    form = SendMessage()
    resp = requests.get(
            apiUrl+'/chat/get-all-user-chat/'+str(json.loads(session.get('user'))['user_id'])+'/'+str(receiver_id),
            headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Bearer " + access_token
            })
    resp = json.loads(resp.text)
    data = resp['data'] if resp['success'] else []
    return render_template('website/views/chat-website.html',chats=data,userId=userId,user = user,receiver_id=receiver_id,form=form)

@website_blueprint.route('/job-detail/<int:event_id>')
@protected_guard
@authInterceptor
def jobDetail(access_token,event_id):
    user =  session.get('user')
    user = json.loads(user) if user else None
    resp = requests.get(
            apiUrl+'/events/get-all-event-by-id/'+str(event_id),
            headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Bearer " + access_token
            })
    resp = json.loads(resp.text)
    eventInfo = resp['data'] if resp['success'] else None
    return render_template('website/views/job-detail.html',user = user, eventInfo = eventInfo, segment='index')

@website_blueprint.route('/user-profile',methods=["GET","POST"])
@authInterceptor
@protected_guard
def userProfile(access_token):
    user =  session.get('user')
    user = json.loads(user) if user else None
    form = UserProfile()
    if form.validate_on_submit():
        body = {
            "user_id":json.loads(session.get('user'))['user_id'],
            "name":form.name.data,
            "city":form.city.data,
            "country":form.country.data,
            "dob":form.dob.data.isoformat(),
            "about":form.about.data,
            "skills":form.skills.data
        }
        resp = requests.post(
        apiUrl+'/users/update-user-info',
        json.dumps(body, indent = 4),
        headers = {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + access_token
        })
        resp = json.loads(resp.text)
        if(resp['success']):
            flash('Update Profile Successfully', 'success')
        else:
            flash('Error on updating profile', 'error')
  
    newResp = requests.get(
        apiUrl+'/users/user-info',
        headers = {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": "Bearer " + access_token
        })
    newResp = json.loads(newResp.text)
    userInfo = newResp['data'] if newResp['success'] else None
    form.name.data = userInfo['name']
    form.city.data = userInfo['city'] if userInfo['city'] else ''
    form.country.data = userInfo['city'] if userInfo['country'] else ''
    form.dob.data = datetime.strptime(userInfo['birthday'], '%Y-%m-%d').date() if userInfo['birthday'] else ''
    form.about.data = userInfo['about'] if userInfo['about'] else ''
    form.skills.data = userInfo['skills'] if userInfo['skills'] else ''
    return render_template('website/views/user-profile.html',user = user,form = form , userInfo=userInfo, segment='index',access_token=access_token)

@website_blueprint.route('/user-application')
@protected_guard
@authInterceptor
def userApplication(access_token):
    form = ReviewForm()
    user =  session.get('user')
    user = json.loads(user) if user else None
    resp = requests.get(
            apiUrl+'/events/view-user-application',
            headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Bearer " + access_token
            })
    resp = json.loads(resp.text)
    data = resp['data'] if resp['success'] else []
    return render_template('website/views/user-application.html',form=form,user = user,data = data, segment='index')

@website_blueprint.route('/volunteer-tasks')
@protected_guard
@authInterceptor
def volunteerTasks(access_token):
    user =  session.get('user')
    user = json.loads(user) if user else None
    body = {
        "page":1,
        "limit":10
    }
    resp = requests.post(
            apiUrl+'/events/get-all-events',
            json.dumps(body, indent = 4),
            headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Bearer " + access_token
            })
    resp = json.loads(resp.text)
    if(resp['success']):
        return render_template('website/views/volunteer-tasks.html',user = user,events=resp['data'],page="events")
    else:
        return render_template('website/views/volunteer-tasks.html',user = user,events=[],page="events")
@website_blueprint.route('/job-apply/<int:event_id>',methods=["GET","POST"])
@protected_guard
@authInterceptor
def jobApply(access_token,event_id):
    user =  session.get('user')
    user = json.loads(user) if user else None
    form =  ApplyJob()

    if form.validate_on_submit():
        file = dict(request.files)['cv']
        image_data = file.read()
        filename = secure_filename(file.filename)  # sanitize the filename
        filePath = 'assets/resume/event-'+str(event_id)+'/user-'+str(json.loads(session.get('user'))['user_id'])
        os.makedirs(filePath, exist_ok=True)
        file.seek(0)
        file.save(os.path.join(filePath, filename))
        # image_base64 = base64.b64encode(image_data).decode('utf-8')
        body = {
            "volunteer_id":json.loads(session.get('user'))['user_id'],
            "event_id":event_id,
            "cover_letter":form.cover_letter.data,
            "cv":filePath,
            "fileName":filename,
            "size": len(image_data)
        }
        resp = requests.post(
            apiUrl+'/events/apply-job-application',
            json.dumps(body, indent = 4),
            headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Bearer " + access_token
            })
        resp = json.loads(resp.text)
        if(resp['success']):
            flash(resp['message'], 'success')
            # return render_template('admin/views/add-update-volunteer-task.html',form=form,resp=resp)
            return redirect('/volunteer-tasks')
        else:
            flash(resp['message'], 'error')
            return redirect('/job-apply/'+str(event_id))
    resp = requests.get(
            apiUrl+'/events/get-all-event-by-id/'+str(event_id),
            headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Bearer " + access_token
            })
    resp = json.loads(resp.text)
    if(resp['success']):
        newResp = requests.get(
            apiUrl+'/users/user-info',
            headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Bearer " + access_token
            })
        newResp = json.loads(newResp.text)
        userInfo = newResp['data'] if newResp['success'] else None
        return render_template('website/views/job-apply.html',user = user, form = form, events = resp['data'],userInfo=userInfo, segment='index')
    else: 
        return render_template('website/views/job-apply.html',user = user,  form = form, events = None,userInfo=None, segment='index')

@website_blueprint.route('/send-message/<int:reciever_id>',methods=['GET','POST'])
@protected_guard
@authInterceptor
def sendMessage(access_token,reciever_id):
    form = SendMessage()
    body = {
        "sender_id":json.loads(session.get('user'))['user_id'],
        "receiver_id":reciever_id,
        "message":form.message.data
    }
    resp = requests.post(
            apiUrl+'/chat/send-message',
            json.dumps(body, indent = 4),
            headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": "Bearer " + access_token
            })
    resp = json.loads(resp.text)
    # data = resp['data'] if resp['success'] else []
    if(resp['success']):
        return redirect('/chat/'+str(reciever_id))
    else:
        flash('Error on sending message','error')
        return redirect('/chat/'+str(reciever_id))
        
@website_blueprint.route('/review/<int:event_id>',methods=['GET','POST'])
@protected_guard
@authInterceptor
def userReview(access_token,event_id):
    user =  session.get('user')
    user = json.loads(user) if user else None
    form = ReviewForm()
    if request.method == 'POST' and form.rating.data == '':
        flash('Rating is required')
    if form.validate_on_submit():
        body = {
            "rating":int(form.rating.data),
            "review":form.review.data,
            "event_id":event_id
        }
        resp = requests.post(
                apiUrl+'/review/add-review',
                json.dumps(body, indent = 4),
                headers = {
                "Content-Type": "application/json; charset=utf-8",
                "Authorization": "Bearer " + access_token
                })
        resp = json.loads(resp.text)
        if(resp['success']):
            flash('Review Added Sucessfully','success')
            return redirect('/user-application')
        else:
            flash('Error on adding review','error')
            return render_template('website/views/review.html',form=form,user = user)

    return render_template('website/views/review.html',form=form,user = user)